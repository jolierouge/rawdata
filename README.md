# Raw Data

This project started as a method of keeping track of raw data spreadsheets that I collect/edit by hand. Getting access to certain
types of data in the world today is exceptionally difficult. Often, you have to record the details of an event as it happens,
as it will soon be forgotten, and import factors erased or simply unremembered.

### How do I get set up? ###

* You will have to have Openoffice/Libre office or any software that can read open document format spreadsheets.

### Contribution guidelines ###

* Your contributions must be fact based, not opinions. Unless you are specifically tracking certain opinions.
* All data is welcome.

### Who do I talk to? ###

* jolierouge242 at gmail